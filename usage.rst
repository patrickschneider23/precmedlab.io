Usage Tutorial
==============

Overview
--------

The PMnet Platform consists of

- A `Workflow Repository <http://gitlab.precmed.iit.demokritos.gr/pipelines>`_.
- A `Tools Repository <http://gitlab.precmed.iit.demokritos.gr/tools>`_.
- A *Workflow Execution Service* based on `Cromwell <https://cromwell.readthedocs.io/>`_.
- A *Storage Service* that implements the S3 protocol and can be browsed using
  `Minio Browser <http://minio.testbed-precmed.iit.demokritos.gr/>`_.

Edit your workflow
------------------

Typically, a workflow description (typically written in `CWL <https://www.commonwl.org/>`_)
reside in the dedicated code repository of the platform. The repository is based
on `Gitlab <https://gitlab.com/gitlab-org/gitlab-ce/>`_ and
supports versioning control via `Git <https://git-scm.com/>`_, issue tracking and
discussion and continuous integration and delivery (CI/CD).

This tutorial assumes familiarity with the typical code development lifecycle.

The current incarnation of the PMnet Workflow Repository can be accessed at
`<http://gitlab.precmed.iit.demokritos.gr>`_. In order to use this repository,
you should:

1. create an account,
2. create a new repository in your account,
3. commit your CWL workflow and CWL tool descriptions.

.. Note::
   At this stage you must commit at a public repository.

A simple definition of a two-step CWL workflow definition can be viewed
`here <http://gitlab.precmed.iit.demokritos.gr/pipelines/demo/raw/master/Simple-Workflow/simple-workflow.cwl>`_.

Upload the inputs files
-----------------------

Use `Minio Browser <http://minio.testbed-precmed.iit.demokritos.gr/>`_ to upload the inputs of your workflow. For these tests upload
your files inside the bucket called ``workflow-inputs``. To avoid filename conflicts
please use a characteristic filename, e.g. ``ncsr_myfile_1.fastq.gz``.

.. Note::
   You should be able to find the credentials of the Storage Service in the `administrativia/credentials <https://gitlab.precmed.iit.demokritos.gr/administrativia/credentials/blob/master/CREDENTIALS.md>`_.

The Storage Service can also be accessed from command line.
You can use the `Minio Client <https://docs.minio.io/docs/minio-client-complete-guide>`_ if you want to
manage the data from the command line. There are platform specific executables available from the linked web page
(e.g. for Linux 64bit), or guidelines on how to install it in your platform (e.g. MacOS). After installation you
need to configure it to use the credentials for the ``precmed`` Storage service, and you can use the following command:

.. code-block:: bash

   mc config host add precmed http://minio.testbed-precmed.iit.demokritos.gr/ <access_key> <secret_key> --api S3v4

Instead of ``precmed`` you could use any name of your liking.

After this, you can, for example, see what's available in the ``workflow-inputs`` bucket doing the following:

.. code-block:: bash

   mc ls precmed/workflow-inputs

You can also upload and download data (using ``cp`` command), retrieve the contents of an "object" (using ``cat``), etc.
See the `Guide <https://docs.minio.io/docs/minio-client-complete-guide>`_ for the complete documentation.

Another option is to use a client side application like for example `CyberDuck <https://cyberduck.io/>`_ and use the S3 
profile. For CyberDuck you need to the `Generic S3 Profile <https://trac.cyberduck.io/wiki/help/en/howto/s3#GenericS3profiles>`_ 
and more specifically download and use the ``S3 (HTTP)`` profile. 

Create the input description
----------------------------

You need to compile an input description file in order to continue with the
submission of your workflow. An input description file collects all the inputs
needed by your workflow and is expressed in `YAML <https://yaml.org/>`_. Input
files are identified by their URLs and you need to construct each URL of the
files you uploaded in the aforementioned step. An input description file will
look similar to the following:

.. code-block:: yaml

    read1:
      class: File
      path: s3://rook-ceph-rgw-my-store.rook-ceph/workflow-inputs/15tu_S1_L001_R1_001.fastq.gz
    read2:
      class: File
      path: s3://rook-ceph-rgw-my-store.rook-ceph/workflow-inputs/15tu_S1_L001_R2_001.fastq.gz
    sample: "15tu_S1"
    rg_name: "G13GV.1"
    seq_center: "MLCDiag"


You can save your input description file locally.

Submit your workflow
--------------------

At this stage, the PMnet platform exposes the RESTful API of the *Workflow Execution Service*
that include functionality for workflow submission. The capabilities of the API
are documented in its `Swagger manifest <http://cromwell.testbed-precmed.iit.demokritos.gr/swagger/index.html?url=/swagger/cromwell.yaml>`_.

For the workflow submission you will need the following terminal command that uses
``cURL``.

.. code-block:: bash

   curl -v "http://cromwell.testbed-precmed.iit.demokritos.gr/api/workflows/v1"\
         -F workflowUrl=http://gitlab.precmed.iit.demokritos.gr/pipelines/demo/raw/master/Simple-Workflow/simple-workflow.cwl \
         -F workflowInputs=@simple-workflow.yml \
         -F workflowType=CWL \
         -F workflowTypeVersion=v1.0

The ``workflowUrl`` is the URL of your workflow. You can construct this URL by
replacing the path name in the following template:

.. code-block:: html

   http://gitlab.precmed.iit.demokritos.gr/<your-gitlab-username>/<your-repository>/raw/master/<your-path-to-the-workflow-cwl-file>


The ``workflowInputs`` is the path for your input description file that reside locally.
The other parameters should remain fixed since you are submitting a CWL workflow.

Upon submission you will see the ``id`` in your terminal. This id can be used
to inspect the progress as follows:

.. code-block:: bash

   curl -v "http://cromwell.testbed-precmed.iit.demokritos.gr/api/workflows/v1/<id>/status"

and to get the URLs to the outputs as follows

.. code-block:: bash

   curl -v "http://cromwell.testbed-precmed.iit.demokritos.gr/api/workflows/v1/<id>/outputs"

Get the results
---------------

Upon completion of your workflow you can access the *Storage Service* using
the `Minio Browser <http://minio.testbed-precmed.iit.demokritos.gr/>`_ and navigate inside the ``workflow-runs`` bucket. The bucket
is organized first by the workflow CWL filename and then by the ``id``.
Inside you will find a folder structure of all intermediate and final outputs.
